# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Старт проекта ###

* git clone git@bitbucket.org:an1ma/anima-stub.git ru.my-project
* cd ru.my-project
* npm install
* bem create -l local/blocks -b page

### Что под капотом? ###

* Bitrix Utilities (в разработке)
* Модули anima.helpers2, anima.bem, [sprint.migration](https://bitbucket.org/andrey_ryabin/sprint.migration)
* Автоматизированная сборка блоков по технологиям на gulp (пока)
* Поддержка [bem-tools](https://github.com/bem/bem-tools) (bem create -l local/blocks)
* [Twitter Bootstrap](http://getbootstrap.com) (twbs)
* [jQuery BEM](https://github.com/hoho/jquery-bem) (блок i-bem)