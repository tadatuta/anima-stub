/**
 * Created by mbakirov on 22.10.14.
 */

$.BEM.decl('i-masked')
    .onMod('js',
        function(mod, val, prev){
            if (val !== 'inited') { return; }

            if(this.$.data('template'))
            {
                this.$.mask(this.$.data('template'));
            }
        }
    );