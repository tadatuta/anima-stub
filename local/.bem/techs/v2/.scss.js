'use strict';

var PATH = require('path'),
    environ = require('bem-environ'),
    compass = require('gulp-compass'),
    BEMCORE_TECHS = PATH.resolve(environ.PRJ_ROOT, 'node_modules/bem/lib/techs/v2');

exports.baseTechPath = require.resolve(BEMCORE_TECHS+'/css-preprocessor.js');

exports.techMixin = {

    template: ['{{bemSelector}}', '{', '}'],

    getBuildSuffixesMap: function() {
        return {
            'sass.css': ['scss', 'css']
        };
    },

    getCreateSuffixes: function() {
        return ['scss'];
    },

    compileBuildResult: function(res, defer) {

        compass({
            sass: 'local/blocks',
            css: 'local/blocks',
            javascript: 'local/blocks',
            image: 'local/img',
            font: 'local/fonts',
            style: 'nested',
            sourcemap: true,
            relative: false,
            comments: true,
            environment: 'development'
        });

    }

};
