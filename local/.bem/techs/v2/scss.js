'use strict';

exports.baseTechPath = require.resolve('./sass.js');

exports.techMixin = {

    template: ['{{bemSelector}}', "{", "}"],

    getBuildSuffixesMap: function() {
        return {
            'scss.css': ['scss', 'css']
        };
    },

    getCreateSuffixes: function() {
        return ['scss'];
    }
};
