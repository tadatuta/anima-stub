<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 21.04.15
 * Time: 14:15
 */

namespace Anima\Bem;


class DependencyInstance {
    var $mustDeps = array();
    var $shouldDeps = array();

    var $blockPath, $blockName;

    function __construct(BlockInstance $block)
    {
        if(file_exists($block->fullPath.'/deps.php'))
        {
            $this->_readDepsPhp($block->fullPath, $block->blockName);
        }

        if(file_exists($block->fullPath.'/'.$block->blockName.'.deps.js'))
        {
            $this->_readDepsJson($block->fullPath, $block->blockName);
        }

        return $this;
    }

    private function _readDepsPhp($blockPath, $blockName)
    {
        $deps = array();
        include_once($blockPath.'/deps.php');
        if(!is_array($deps))
            throw new \Bitrix\Main\NotSupportedException("%s block deps.php file must contains array", $blockName);

        foreach($deps as $k => $dependency)
        {
            $dependencyObj = new \stdClass();
            $dependencyObj->block = $dependency;
            $this->mustDeps[] = $dependencyObj;
        }
    }

    private function _readDepsJson($blockPath, $blockName)
    {
        $object = Json::readFile($blockPath.'/'.$blockName.'.deps.js');
        if(!is_object($object))
            throw new \Bitrix\Main\NotSupportedException("%s block deps.php file must contains array", $blockName);

        $this->mustDeps = $object->mustDeps;
        $this->shouldDeps = $object->shouldDeps;
    }

    function getMustArray()
    {
        $return = array();
        foreach ($this->mustDeps as $dependency) {
            $name = $dependency->block;
            if($dependency->elem)
            {
                $name = $name.'__'.$dependency->elem;
            }
            $return[] = $name;
        }

        return $return;
    }

    function getShouldArray()
    {
        $return = array();
        foreach ($this->shouldDeps as $dependency) {
            $name = $dependency->block;
            if($dependency->elem) {
                $name = $name . $dependency->elem;
            }
            $return[] = $name;
        }

        return $return;
    }
}