<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 27.10.14
 * Time: 6:30
 */

namespace Anima\Bem;

/**
 * Class Proxy
 * @package Anima\Bem
 */
class Proxy {
    public static function loadComponent($componentName, $componentTemplate, $arParams = array(), $parentComponent = null, $arFunctionParams = array())
    {
        return $GLOBALS['APPLICATION']->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
    }
}