<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 27.10.14
 * Time: 6:30
 */

namespace Anima\Bem;

/**
 * Class Block
 * @package Anima\Bem
 */
class Block {
    /**
     * @var $instance Block keeps instance reference
     * @var $blocksDir string of path for blocks
     * @var $objBlocks array of BlockInstances
     */
    static $instance;
    private $objBlocks;
    private $blocksDir = '/local/blocks/';

    /**
     * Constructor
     */
    function __construct() {
        $this->getBlocksList();
    }

    /**
     * Get instance of Block class
     * @return Block
     */
    public static function getInstance() {
        if (is_null(self::$instance))
        {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Init core
     *
     * @return bool
     */
    private function initCore() {
        $this->loadBlockFiles('i-bem');
        return true;
    }

    /**
     * Grab blocks folders from $blocksDir
     *
     * @return Block
     */
    private function getBlocksList()
    {
        $blockSearchDir = $_SERVER['DOCUMENT_ROOT'].$this->blocksDir;
        if(is_dir($blockSearchDir))
        {
            foreach(scandir($blockSearchDir) as $key => $__blockName)
            {
                $blockName = strtolower($__blockName);
                $blockPath = $blockSearchDir.$blockName;

                if($key < 2) continue;

                if(is_dir($blockPath))
                {
                    $this->objBlocks[$blockName] = new BlockInstance($blockName);
                    foreach($this->objBlocks[$blockName]->elems as $elemName)
                    {
                        $this->objBlocks[$blockName.'__'.$elemName] = new ElemInstance($blockName, $elemName);
                    }
                }
            }
        }

        return $this;
    }

    /**
     * Loads blocks files in runtime
     *
     * @param string $blockName
     * @param bool $bFullLoad
     * @return bool
     */
    private function loadBlockFilesFromJson($blockName, $bFullLoad = false) {
        /** @var BlockInstance $block */
        $block = $this->objBlocks[$blockName];

        if(!$block) { return false; }

        foreach($block->deps->getMustArray() as $depName)
        {
            $this->loadBlockFilesFromJson($depName);
        }

        $block->load();

        if($bFullLoad && !empty($block->elems))
        {
            foreach($block->elems as $k => $elemName)
            {
                $this->loadBlockFilesFromJson($block->blockName.'__'.$elemName);
            }
        }

        foreach($block->deps->getShouldArray() as $depName)
        {
            $this->loadBlockFilesFromJson($depName);
        }

        return true;
    }

    /**
     * Logic method to init block. Inits core, if necessary.
     *
     * @param \stdClass $blockJson
     * @return bool
     */
    private function initBlockFromJson($blockJson) {
        $this->initCore();

        $blockName = $blockJson->block;
        $bFullLoad = true;
        if($blockJson->elem)
        {
            $blockName .= '__'.$blockJson->elem;
            $bFullLoad = false;
        }

        $block = $this->objBlocks[$blockName];

        if(!is_object($block) || $block->isLoaded) {
            return false;
        }

        $this->loadBlockFilesFromJson($blockName, $bFullLoad);

        return true;
    }

    /**
     * Static wrapper to initBlock
     *
     * @param json $blocksJson
     * @return bool
     */
    public static function loadFromJson($blocksJson) {
        $instance = self::getInstance();

        $blocks = Json::read($blocksJson);

        foreach($blocks as $blockJson) {
            $instance->initBlockFromJson($blockJson);
        }
        return true;
    }

    public static function load($blockName) {
        $json = '';
        if(is_array($blockName))
        {
            $blockNameArray = $blockName;
        }
        else
        {
            $blockNameArray = array($blockName);
        }
        foreach($blockNameArray as $k => $name)
        {
            $json .= "{block:'{$name}'},";
        }
        $json = "[".trim($json, " \t\n\r\0\x0B,")."]";
        return self::loadFromJson($json);
    }

    private function addBlockPath($path) {
        return true;
    }

    public static function addPath($path) {
        return true;
    }

    private function initBlock($__blockName) {
        return true;
    }

    private function loadBlockFiles($blockName) {
        return $this->loadBlockFilesFromJson($blockName);
    }

    private function init() {
        return true;
    }
}