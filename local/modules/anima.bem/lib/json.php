<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 21.04.15
 * Time: 15:05
 */

namespace Anima\Bem;

class Json {
    static function read($string)
    {
        $json = self::fix(trim($string," \t\n\r\0\x0B();"));
        return json_decode($json);
    }

    static function readFile($path)
    {
        $string = file_get_contents($path);
        return self::read($string);
    }

    static function fix( $j ){
        $j = trim( $j );
        $j = ltrim( $j, '(' );
        $j = rtrim( $j, ')' );
        $a = preg_split('#(?<!\\\\)\"#', $j );
        for( $i=0; $i < count( $a ); $i+=2 ){
            $s = $a[$i];
            $s = preg_replace('#([^\s\[\]\{\}\:\,]+):#', '"\1":', $s );
            $a[$i] = $s;
        }
        //var_dump($a);
        $j = implode( '"', $a );
        $j = str_replace("'", '"', $j);
        //var_dump( $j );
        return $j;
    }
}