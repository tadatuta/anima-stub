<?
\Bitrix\Main\Loader::registerAutoLoadClasses(
	"anima.bem",
	array(
        "Anima\\Bem\\BlockInstance" => "lib/blockInstance.php",
        "Anima\\Bem\\ElemInstance" => "lib/elemInstance.php",
        "Anima\\Bem\\DependencyInstance" => "lib/dependencyInstance.php",
		"Anima\\Bem\\Block" => "lib/block.php",
		"Anima\\Bem\\Proxy" => "lib/proxy.php",
		"Anima\\Bem\\Common" => "lib/common.php",
		"Anima\\Bem\\Json" => "lib/json.php",
	)
);