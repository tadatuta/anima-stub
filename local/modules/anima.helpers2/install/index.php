<?
IncludeModuleLangFile(__FILE__);

if (class_exists("anima_helpers2"))
    return;


class anima_helpers2 extends CModule
{
    public $PARTNER_NAME;
    public $PARTNER_URI;
    var $MODULE_ID = "anima.helpers2";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function __construct()
    {
        $arModuleVersion = array();

        include(dirname(__FILE__)."/version.php");

        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];

        $this->MODULE_NAME = GetMessage("ANIMA_HELPERS_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("ANIMA_HELPERS_MODULE_DESC");

        $this->PARTNER_NAME = GetMessage("ANIMA_HELPERS_PARTNER_NAME");
        $this->PARTNER_URI = 'http://anima.ru';
    }

    function InstallDB($arParams = array())
    {
        \COption::SetOptionInt("anima.helpers2", "autoload_module", 1);
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        \COption::RemoveOption("anima.helpers2", "autoload_module");
        return true;
    }

    function InstallEvents()
    {
        RegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Anima\\Helpers\\Common", "init");
        return true;
    }

    function UnInstallEvents()
    {
        UnRegisterModuleDependences("main", "OnBeforeProlog", $this->MODULE_ID, "Anima\\Helpers\\Common", "init");
        return true;
    }

    function InstallFiles($arParams = array())
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

    function DoInstall()
    {
        global $APPLICATION;
        RegisterModule($this->MODULE_ID);
        $this->InstallFiles();
        $this->InstallDB();
        $this->InstallEvents();
    }

    function DoUninstall()
    {
        global $APPLICATION;
        $this->UnInstallEvents();
        $this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }
}
?>
