<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 17.03.14
 * Time: 1:18
 */

namespace Anima\Helpers;

/**
 * Class Common
 * @package Anima\Helpers
 */
class Common {
    /**
     * method, that fires on each page hit when module installed
     */
    public static function init() {
        if(\COption::GetOptionString("anima.helpers", "autoload_module", 1)) {
            self::autoLoad();
        }
    }

    /**
     * Autoloads helpers module
     *
     * @return bool
     */
    public static function autoLoad() {
        return \Bitrix\Main\Loader::includeModule('anima.helpers');
    }
}