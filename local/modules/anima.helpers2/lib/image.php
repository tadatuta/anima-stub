<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 17.03.14
 * Time: 1:08
 */

namespace Anima\Helpers;

/**
 * Class Image
 * @package Anima\Helpers
 */
class Image {
    /**
     * Resize image to contain it in sizes
     *
     * @since 1.1.0
     *
     * @param mixed $rsPicture picture ID or picture array
     * @param array $arResizeProportions array('height' => 100, 'width' => 100)
     * @param constant int $typeOfResize
     * @return array()
     */
    public static function contain($rsPicture, $arResizeProportions, $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL) {
        if(empty($arResizeProportions)) return false;

        $arFile = self::normalizePicture($rsPicture);

        if(empty($arFile)) {
            return array();
        }

        if($arFile["WIDTH"] > $arResizeProportions["width"] || $arFile["HEIGHT"] > $arResizeProportions["height"]) {
            if(!$arResizeProportions["width"]) {
                $props = $arFile["HEIGHT"] / $arResizeProportions['height'];
                $arResizeProportions["width"] = ceil($arFile["WIDTH"] / $props);
            }
            if(!$arResizeProportions["height"]) {
                $props = $arFile["WIDTH"] / $arResizeProportions['width'];
                $arResizeProportions["height"] = ceil($arFile["HEIGHT"] / $props);
            }
        } else {
            return $arFile;
        }

        if($typeOfResize != BX_RESIZE_IMAGE_EXACT) {
            if($arResizeProportions['height'] > $arResizeProportions['width']) {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL_ALT;
            } else {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL;
            }
        }

        return self::resizeExact($arFile, $arResizeProportions, $typeOfResize);
    }

    /**
     * Compatibility wrapper for self::contain
     *
     * @param mixed $rsPicture picture ID or picture array
     * @param array $arResizeProportions array('height' => 100, 'width' => 100)
     * @param constant int $typeOfResize
     * @return array
     */
    public static function resizeByMaxSize($rsPicture, $arResizeProportions, $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL) {
        return self::contain($rsPicture, $arResizeProportions, $typeOfResize);
    }

    /**
     * Resize image to cover sizes
     *
     * @since 1.1.0
     *
     * @param $rsPicture
     * @param $_arResizeProportions
     * @param int $typeOfResize
     * @return array
     */
    public static function cover($rsPicture, $_arResizeProportions, $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL) {
        if(
            empty($_arResizeProportions)
            ||
            !array_key_exists("width", $_arResizeProportions)
            ||
            !array_key_exists("height", $_arResizeProportions)
        ) return false;

        $arFile = self::normalizePicture($rsPicture);

        if(empty($arFile)) {
            return array();
        }


        $arResizeProportions['width'] = $_arResizeProportions['width'];
        $arResizeProportions["height"] = ceil($arResizeProportions['width'] * $arFile["HEIGHT"] / $arFile["WIDTH"]);

        if($arResizeProportions["height"] < $_arResizeProportions['height']) {
            $arResizeProportions['height'] = $_arResizeProportions['height'];
            $arResizeProportions["width"] = ceil($arResizeProportions['height'] * $arFile["WIDTH"] / $arFile["HEIGHT"]);
        }
        //$bMinDimensions = true;

        if($typeOfResize != BX_RESIZE_IMAGE_EXACT) {
            if($arResizeProportions['height'] > $arResizeProportions['width']) {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL_ALT;
            } else {
                $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL;
            }
        }

        $arFile = self::resizeExact($arFile, $arResizeProportions, $typeOfResize);

        $arFile['ADD_CSS'] = '';
        $arFile['ARR_ADD_CSS'] = array();
        if($arFile["HEIGHT"] > $arResizeProportions['min-height']) {
            $arFile['ARR_ADD_CSS']['TOP'] = ceil(-1*($arFile["HEIGHT"]-$_arResizeProportions['height'])/2).'px';
            $arFile['ADD_CSS'].='margin-top:'.ceil(-1*($arFile["HEIGHT"]-$_arResizeProportions['height'])/2).'px;';
        }
        if($arFile['WIDTH'] > $arResizeProportions['min-width']) {
            $arFile['ARR_ADD_CSS']['LEFT'] = ceil(-1*($arFile["WIDTH"]-$_arResizeProportions['width'])/2).'px';
            $arFile['ADD_CSS'].='margin-left:'.ceil(-1*($arFile["WIDTH"]-$_arResizeProportions['width'])/2).'px;';
        }

        return $arFile;
    }

    /**
     * Compatibility wrapper for self::cover
     *
     * @param mixed $rsPicture picture ID or picture array
     * @param array $arResizeProportions array('height' => 100, 'width' => 100)
     * @param constant int $typeOfResize
     * @return array
     */
    public static function resizeByMinSize($rsPicture, $arResizeProportions, $typeOfResize = BX_RESIZE_IMAGE_PROPORTIONAL) {
        return self::cover($rsPicture, $arResizeProportions, $typeOfResize);
    }

    /**
     * Normalizing picture from ID or array
     *
     * @param mixed $rsPicture picture ID or picture array
     * @return array
     */
    public static function normalizePicture($rsPicture) {
        if(is_array($rsPicture) && intval($rsPicture["WIDTH"]) > 0 && intval($rsPicture["HEIGHT"]) > 0 && intval($rsPicture["ID"]) > 0 && strlen($rsPicture["SRC"]) > 0) {
            $arFile = $rsPicture;
        } elseif(!is_array($rsPicture) && intval($rsPicture) > 0) {
            $arFile = \CFile::GetFileArray(intval($rsPicture));
        } else {
            return array();
        }

        return $arFile;
    }

    /**
     * Resize process
     *
     * @param $arFile
     * @param $arResizeProportions
     * @param $typeOfResize
     * @return array
     */
    public static function resizeExact($arFile, $arResizeProportions, $typeOfResize) {
        $arResize = \CFile::ResizeImageGet(
            $arFile["ID"],
            array(
                'width' => $arResizeProportions['width'],
                'height' => $arResizeProportions['height']
            ),
            $typeOfResize,
            true
        );

        $arFile["WIDTH"] = $arResize["width"];
        $arFile["HEIGHT"] = $arResize["height"];
        $arFile["ORIGINAL_SRC"] = $arFile["SRC"];
        $arFile["SRC"] = $arResize["src"];

        return $arFile;
    }
}
