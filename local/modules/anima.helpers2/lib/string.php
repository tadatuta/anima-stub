<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 17.03.14
 * Time: 1:08
 */

namespace Anima\Helpers;
IncludeModuleLangFile(__FILE__);

/**
 * Class String
 * @package Anima\Helpers
 */
Class String {
    /**
     * Капитализация первого символа в русской строке
     *
     * @since 1.0.0
     *
     * @param string $string
     * @param string $encoding
     * @return string
     */
    public static function ucfirst($string, $encoding = 'UTF-8') {
        return mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding).mb_substr($string, 1, mb_strlen($string, $encoding), $encoding);
    }

    /**
     * Кодировка в JSON с поддержкой русских символов
     *
     * @since 1.1.0
     *
     * @param $str
     * @return string json
     */
    static function json_encode($str) {
        $arr_replace_utf = array('\u0410', '\u0430','\u0411','\u0431','\u0412','\u0432',
            '\u0413','\u0433','\u0414','\u0434','\u0415','\u0435','\u0401','\u0451','\u0416',
            '\u0436','\u0417','\u0437','\u0418','\u0438','\u0419','\u0439','\u041a','\u043a',
            '\u041b','\u043b','\u041c','\u043c','\u041d','\u043d','\u041e','\u043e','\u041f',
            '\u043f','\u0420','\u0440','\u0421','\u0441','\u0422','\u0442','\u0423','\u0443',
            '\u0424','\u0444','\u0425','\u0445','\u0426','\u0446','\u0427','\u0447','\u0428',
            '\u0448','\u0429','\u0449','\u042a','\u044a','\u042b','\u044b','\u042c','\u044c',
            '\u042d','\u044d','\u042e','\u044e','\u042f','\u044f');
        $arr_replace_cyr = explode(',', GetMessage('ANIMA_HELPERS_RUSSIAN_LETTERS'));
        $str1 = json_encode($str);
        $str2 = str_replace($arr_replace_utf,$arr_replace_cyr,$str1);
        return $str2;
    }

    /**
     * Возведение существительного в множественное число
     *
     * @since 1.1.1
     *
     * @param int $howmuch
     * @param array $input Например: array('товар', 'товара', 'товаров') – [0] - товар (единственное число, именительный падеж), [1] - товара (единственное число, родительный падеж), [2] - товаров (множественное число, родительный падеж)
     * @return string
     */
    static public function plural($howmuch, $input) {
        $howmuch = (int)$howmuch;
        $result = '';

        $l2 = substr($howmuch,-2);
        $l1 = substr($howmuch,-1);
        if($l2 > 10 && $l2 < 20) return $input[2];
        else
            switch ($l1) {
                case 0: $result = $input[2]; break;
                case 1: $result = $input[0]; break;
                case 2: case 3: case 4: $result = $input[1]; break;
                default: $result = $input[2]; break;
            }
        return $result;
    }
}