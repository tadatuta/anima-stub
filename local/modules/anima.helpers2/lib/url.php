<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 17.03.14
 * Time: 1:09
 */

namespace Anima\Helpers;

/**
 * Class Url
 * @package Anima\Helpers
 */
class Url {
    /**
     * Convert full URL paths to absolute paths.
     *
     * Removes the http or https protocols and the domain. Keeps the path '/' at the
     * beginning, so it isn't a true relative link, but from the web root base.
     *
     * @since 1.0.0
     *
     * @param string $link Full URL path.
     * @return string Absolute path.
     */
    static public function makeLinkRelative( $link ) {
        return preg_replace( '|https?://[^/]+(/.*)|i', '$1', $link );
    }
}